<!DOCTYPE html>
<html lang="es">
<head>
<title>Master en PHP!</title>
<meta charset="utf-8">
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
        background-color: #181A1B;
        color: #FFF;
    }
</style>
</head>
<body>
<h1>Welcome to racmol!</h1>

<h2>Formulario PHP</h2>
<form method="post" action="form/recibir.php">
    <p>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" />
    </p>
    <p>
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos" />
    </p>

    <input type="submit" value="Enviar datos"/>
</form>

</body>
</html>

<?php
/*
echo "OK";

echo "<h1>".$_POST['nombre']."</h1>";
echo "<h1>".$_POST['apellidos']."</h1>";

var_dump($_POST);
*/
?>
