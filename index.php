<!DOCTYPE html>
<html lang="es">
<head>
<title>Master en PHP!</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>
<h1>Welcome to racmol!</h1>

<div class="cabecera">
    <h1>Includes en PHP</h1>
    <ul>
        <li><a href="index.php"></a>Inicio</li>
        <li><a href="sobremi.php"></a>Sobre mi</li>
        <li><a href="contacto.php"></a>Contacto</li>
    </ul>
    <hr>
</div>

<!--Contenido-->
<div>
    <h2>Esta es la pagina de inicio</h2>
    <p>Texto de prueba de la pagina de inicio</p>
</div>

<!--Pie de pagina-->
<footer>
    <hr>
    Todos los derechos reservados &copy; Carlos Molina <?=date('Y')?>
</footer>

<?php



?>

</body>
</html>
