<?php
/*
Ejercio 6. Tablas de multiplicar del 1 al 10
*/

for($i=1; $i<=10; $i++){
    echo "<table border='1'>"; //Inicio de tabla
    echo "<tr><th>Tabla del ".$i . "</th></tr>";
    for($j=0; $j<=10; $j++){
        $res = $i*$j;
        echo "<tr><td>" . $i . " x " . $j . " = " . $res . "</td></tr>";
    }
    echo "</table>";
    //echo "<br>";
}

?>