<?php
/*
Ejercio 5 Bloque 2.
Crear un array con el contenido de la tabla:
ACCION  AVENTURA    DEPORTES
GTA     ASSASINS    FIFA 19
COD     CRASH       PES 19
PUGB    PRINCE      MOTO GP

Cada fila debe ir en un fichero separado(includes).
*/

$tabla = array(
    "ACCION" => array("GTA", "COD", "PUGB"),
    "AVENTURA" => array("ASSASINS", "CRASH", "PRINCE"),
    "DEPORTES" => array("FIFA 19", "PES 19", "MOTO GP")
);

$categorias = array_keys($tabla);

?>

<table border="1">
    
<?php require_once "eII5/encabezados.php" ?>
<?php require_once "eII5/primera.php" ?>
<?php require_once "eII5/segunda.php" ?>
<?php require_once "eII5/tercera.php" ?>
    
    
</table>
