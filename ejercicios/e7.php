<?php
/*
Ejercio 7. Hacer un programa que muestre todos los numeros impares
* entre dos numeros que nos lleguen por la url($_GET)
*/

if(isset($_GET['numero1']) && isset($_GET['numero2'])){
    $numero1 = $_GET['numero1'];
    $numero2 = $_GET['numero2'];

    echo "<h3>Los números impares que están entre ".$numero1." y " . $numero2 . " son: <br>";
    if($numero1<$numero2){
        for($i=$numero1; $i<=$numero2; $i++){
            if($i%2 == 1)
                echo "$i<br>";
        }
    }
    else{
        echo "numero 1 debe ser mayor al numero 2";
    }
}
else{
    echo "<h3>Introduce correctamente los valores por la URL";
}


?>