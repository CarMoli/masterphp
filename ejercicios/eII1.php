<?php
/*
Ejercio 1 Bloque 2. 
Hacer un programa en PHP que tenga un array con 8 numeros enteros
y que haga lo siguiente:
- Recorrerlo y mostrarlo
- Ordenarlo y mostrarlo
- Mostrar su longitud
- Buscar algun elemento (buscar por elemento que llegue en la url)
*/

$numeros = array('8','12','20','1','53','38','13','7');

echo "El arreglo \$numeros tiene los siguientes elementos: <br>";
foreach($numeros as $x){
    echo $x . '<br>';
}
echo '<br>El arreglo ordenado queda como: <br>';
sort($numeros);
foreach($numeros as $x){
    echo $x . '<br>';
}

$longitud = count($numeros);
echo '<br>El arreglo tiene una longitud de: <br>';
echo $longitud . " elementos";

if(isset($_GET['num'])){
    $busqueda = $_GET['num'];
    $search = array_search($busqueda, $numeros);

    if(!empty($search)){
        echo "<br>El número buscado existe en el array en el indice $search<br>";
    }
    else{
        echo "<br>El número buscado no existe en el array<br>";
    }
}

?>