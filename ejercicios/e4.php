<?php
/*
Ejercio 4. Recoger dos numeros por la url(Parámetros GET) y
* hacer todas las operaciones básicas de una calculadora
* (suma resta multiplicacion division) de estos numeros.
*/

if(isset($_GET['numero1']) && isset($_GET['numero2'])){
    $numero1 = $_GET['numero1'];
    $numero2 = $_GET['numero2'];

    $resultado=$numero1+$numero2;
    echo "La suma de ".$numero1." + ".$numero2." es: ".$resultado."<br>";

    $resultado=$numero1-$numero2;
    echo "La resta de ".$numero1." - ".$numero2." es: ".$resultado."<br>";

    $resultado=$numero1*$numero2;
    echo "La multiplicacion de ".$numero1." * ".$numero2." es: ".$resultado."<br>";

    $resultado=$numero1/$numero2;
    echo "La division de ".$numero1." / ".$numero2." es: ".$resultado."<br>";
}
else{
    echo "<h3>Introduce correctamente los valores por la URL";
}


?>