<?php

/**
 * Ejercicio 1. Crear una sesión que aumente su valor en uno o disminuya
 * en uno, en función de si el parámetro get counter está en uno o a cero.
 */

 session_start();

 if(!isset($_SESSION['valor'])){
    $_SESSION['valor'] = 0;
 }

 if(isset($_GET['counter']) && $_GET['counter']==0){
     $_SESSION['valor']++;
 }

 if(isset($_GET['counter']) && $_GET['counter']==1){
    $_SESSION['valor']--;
}

?>
<h2>El valor de la sesion es: <?= $_SESSION['valor'] ?></h2>
<a href="eIII1.php?counter=1">Disminuir</a><br/>
<a href="eIII1.php?counter=0">Aumentar</a>