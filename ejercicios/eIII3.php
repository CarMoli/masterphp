<?php
/**
 * Ejercicio 3.
 * Hacer una interfáz de usuario (formulario) con dos inputs y
 * 4 botones uno para sumar, restar, dividir y multiplicar.
 */

/*
$resultado = false;
if(isset($_GET["num1"], $_GET["num2"])){
  $num1=(int)$_GET["num1"];
  $num2=(int)$_GET["num2"];
  if(isset($_GET["sumar"])){
    $resultado="El resultado es: ".$num1+$num2;
  }
  if(isset($_GET["restar"])){
    $resultado="El resultado es: ".$num1-$num2;
  }
  if(isset($_GET["dividir"])){
    $resultado="El resultado es: ".$num1 / $num2;
  }
  if(isset($_GET["multi"])){
    $resultado="El resultado es: ".$num1*$num2;
  }
}
 */
?>
<!DOCTYPE HTML>
<head>
  <meta charset="UTF-8"/>
  <title>Ejercicio 3 del Bloque 3</title>
</head>
<body>
  <h1>Calculadora</h1>
  <form action="calcula.php" method="GET">
    <label for="num1">Numero 1</label>
    <input type="number" name="num1"></br></br>
    <label for="num2">Numero 2</label>
    <input type="number" name="num2"></br></br>
    <input type="submit" value="Suma" name="sumar">
    <input type="submit" value="Resta" name="restar">
    <input type="submit" value="Divide" name="dividir">
    <input type="submit" value="Multiplica" name="multi">
  </form>
<?php
/*
if($resultado != false):
  echo $resultado;
endif;
 */
?>
</body>
