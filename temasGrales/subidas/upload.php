<?php
error_reporting(E_ALL);

$archivo = $_FILES['archivo'];
$nombre = $archivo["name"];
$tipo = $archivo['type'];
$error = $archivo['error'];

if($tipo == 'image/jpg' || $tipo == 'image/jpeg' || $tipo == 'image/png' || $tipo == 'image/gif'){
    if(!is_dir('images')){
        mkdir('images');
    }
    chmod('images/', 0777);
    move_uploaded_file($archivo['tmp_name'], './images/'.$nombre);
    header('Refresh: 3; URL=index.php');
    echo '<h2>Subida exitosa</h2>';
}else{
    header('Refresh: 3; URL=index.php');
    echo '<strong>Sube una imagen con un formato correcto, por favor.</strong>';
}