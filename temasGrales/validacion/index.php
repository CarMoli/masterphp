<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validación de formularios</title>
</head>
<body>
    <h1>Validar formularios en PHP</h1>

    <?php
        if(isset($_GET['error'])){
            $error = $_GET['error'];
            if($error == 'faltan_valores'){
                echo '<strong style="color:red">Introduce todos los datos en todos los campos del formulario </strong>';
            }
            if($error == 'nombre'){
                echo '<strong style="color:red">Introduce correctamente el nombre</strong>';
            }
            if($error == 'apellidos'){
                echo '<strong style="color:red">Introduce correctamente el apellido</strong>';
            }
            if($error == 'edad'){
                echo '<strong style="color:red">Introduce correctamente la edad</strong>';
            }
            if($error == 'email'){
                echo '<strong style="color:red">Introduce correctamente el email</strong>';
            }
            if($error == 'pass'){
                echo '<strong style="color:red">Introduce una contraseña de más de 5 caracteres</strong>';
            }
        }
    ?>

    <form action="procesar.php" method="POST">
    <label for="nombre">Nombre</label><br>
        <input type="text" name="nombre" id="" required="required" pattern="[A-Za-z ]+"><br>

        <label for="apellidos">Apellidos</label><br>
        <input type="text" name="apellidos" id="" pattern="[A-Za-z ]+"><br>

        <label for="edad">Edad</label><br>
        <input type="number" name="edad" id="" pattern="[0-9]+"><br>

        <label for="email">Email</label><br>
        <input type="email" name="email" id="" required="required"><br>

        <label for="pass">Contraseña</label><br>
        <input type="password" name="pass" id="" required="required"><br>

        <input type="submit" value="Enviar">
    </form>
</body>
</html>