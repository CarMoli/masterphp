<!DOCTYPE html>
<html lang="es">
<head>
<title>Master en PHP!</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="../../estilos.css">
</head>
<body>
<h1>Welcome to racmol!</h1>

<?php
    $nombre = "Juan Gutierrez";
?>

<div class="cabecera">
    <h1>Includes en PHP</h1>
    <ul>
        <li><a href="index.php">Inicio</a></li>
        <li><a href="sobremi.php">Sobre mi</a></li>
        <li><a href="contacto.php">Contacto</a></li>
    </ul>
    <hr>
</div>