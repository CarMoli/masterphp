<?php
    /*
    SESION: Almacenar y persistir datos del usuario mientras que
    navega en un sitio web hasta que cierra sesión o cierra el
    navegador.
     */
    session_start();

    //variable local
    $var = "soy una cadena de texto";

    //variable de sesión
    $_SESSION['variable_persistente'] = "Hola soy una sesion activa";

    echo $var . "<br>";
    echo $_SESSION['variable_persistente'];
    $_SESSION['color'] = 'green';
    $_SESSION['comida'] = 'enchiladas';
?>
<br/>
<a href="pagina1.php">Página 1</a>
<br/>
<a href="logout.php">Logout</a>