# RENOMBRAR UNA TABLA
ALTER TABLE blog_master.usuarios_renom
RENAME TO usuarios;

ALTER TABLE usuarios
RENAME TO usuarios_renom;

# CAMBIAR NOMBRE DE UNA COLUMNA
ALTER TABLE usuarios 
CHANGE apellidos apellido varchar(100) null;

-- Postgre
ALTER TABLE usuarios
RENAME apellidos TO ape;

# MODIFICAR COLUMNA SIN CAMBIAR NOMBRE 
ALTER TABLE usuarios 
MODIFY apellido
char(40) not null;

-- Postgre
ALTER TABLE usuarios
ALTER COLUMN apellidos 
TYPE char(40);

# AÑADIR COLUMNA
ALTER TABLE usuarios
ADD website varchar(100) null;

-- Postgre
ALTER TABLE usuarios
ADD COLUMN website varchar(100);

# AÑADIR RESTRICCIÓN A COLUMNA
ALTER TABLE usuarios
ADD CONSTRAINT uq_mail
UNIQUE(email);

-- Postgre
ALTER TABLE usuarios
ADD CONSTRAINT uq_mail 
UNIQUE(email);

# BORRAR UNA COLUMNA
ALTER TABLE usuarios
DROP website;

-- Postgre
ALTER TABLE usuarios
DROP COLUMN website;
