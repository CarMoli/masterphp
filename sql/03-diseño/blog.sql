-- MariaDB
CREATE TABLE usuarios(
id		int(255) auto_increment NOT NULL,
nombre		varchar(100) NOT NULL,
apellidos 	varchar(100) NOT NULL,
email 		varchar(200) NOT NULL,
password 	varchar(255) NOT NULL,
fecha 		date NOT NULL,
CONSTRAINT pk_usuarios PRIMARY KEY(id),
CONSTRAINT uq_email UNIQUE(email)
)ENGINE=InnoDb;

-- Postgre
CREATE TABLE IF NOT EXISTS usuarios(
  id 		SERIAL NOT NULL,
  nombre 	VARCHAR(100) NOT NULL,
  apellidos 	VARCHAR(100) NOT NULL,
  email 	VARCHAR(200) NOT NULL,
  password 	VARCHAR(255) NOT NULL,
  fecha 	DATE NOT NULL,
  CONSTRAINT pk_usuarios PRIMARY KEY(id),
  CONSTRAINT uq_email UNIQUE(email)
);

-- MariaDB
CREATE TABLE categorias(
id 	int(255) AUTO_INCREMENT NOT NULL,
nombre 	varchar(100),
CONSTRAINT pk_categorias PRIMARY KEY(id)
)ENGINE=InnoDb;

-- Postgre
CREATE TABLE IF NOT EXISTS categorias(
id 	SERIAL NOT NULL,
nombre 	VARCHAR(100),
CONSTRAINT pk_categorias PRIMARY KEY(id)
);

-- MariaDB
CREATE TABLE entradas(
id              int(255) AUTO_INCREMENT NOT NULL,
usuario_id      int(255) NOT NULL,
categoria_id    int(255) NOT NULL,
titulo          varchar(255) NOT NULL,
descripcion     MEDIUMTEXT,
fecha           date NOT NULL,
CONSTRAINT pk_entradas PRIMARY KEY(id),
CONSTRAINT fk_entradas_usuario FOREIGN KEY(usuario_id) REFERENCES usuarios(id),
CONSTRAINT fk_entradas_categoria FOREIGN KEY(categoria_id) REFERENCES categorias(id)
)ENGINE=InnoDb;

-- Postgre
CREATE TABLE entradas(
id 		SERIAL NOT NULL,
usuario_id 	SERIAL NOT NULL,
categoria_id 	SERIAL NOT NULL,
titulo 		VARCHAR(255) NOT NULL,
descripcion 	TEXT,
fecha 		DATE NOT NULL,
CONSTRAINT pk_entradas PRIMARY KEY(id),
CONSTRAINT fk_entradas_usuario FOREIGN KEY(usuario_id) REFERENCES usuarios(id),
CONSTRAINT fk_entradas_categoria FOREIGN KEY(categoria_id) REFERENCES categorias(id)
);
