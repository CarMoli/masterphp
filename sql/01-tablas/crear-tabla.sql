/* MySQL y MaraiaDB*/
create table usuarios(
id int(11) auto_increment not null,
nombre varchar(50) not null,
apellidos varchar(100) default 'hola que tal',
email varchar(100) not null,
password varchar(255),
CONSTRAINT pk_usuarios PRIMARY KEY(id)
);

-- PostgreSQL
CREATE TABLE usuarios(
id SERIAL not null PRIMARY KEY, 
nombre varchar(50) not null, 
apellidos varchar(100) default 'hola que tal', 
email varchar(100) not null, 
password varchar(255)
);
